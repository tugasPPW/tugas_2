from django.shortcuts import render
from django.urls import reverse
from a4.models import Teman
from a2.models import *
from django.forms.models import model_to_dict
from django.http import *
from .utils import *
import requests
from .forms import Status_Form


# Create your views here.
response = {}
def index(request): # pragma: no cover
    response['author'] = "Muhammad Ikhsan Kurniawan"
    response['riwayats'] = get_riwayat()
    html = "a2/a2.html"
    friends = Teman.objects.all()
    friend_list = []

    for friend in friends:
        friend_list.append(model_to_dict(friend))

    # daftar = {'friend_list' : friend_list}
    response['friend_list'] = friend_list
    kode_identitas = get_data_user(request, 'kode_identitas')
    user_exist = check_user_in_database(request, kode_identitas)
    if (user_exist):
    	user = Teman.objects.get(npm = kode_identitas)
    else:
    	user = create_new_user(request)
    pengguna = Teman.objects.get(npm = kode_identitas)
    response['status'] = pengguna.status_set.all()
    # print(response['status'])
    stat = Status.objects.all().order_by('-id')
    if (len(stat) > 0):
        message = stat[0]
        newMessage = message.status
        time = message.created_date
    else:
        newMessage = "You have not posted yet"
        # time = date(datetime.now().year,datetime.now().month,datetime.now().day)
    response['latestMessage'] = newMessage
    response['jumlah_status'] = pengguna.status_set.count()
    response['daftar_riwayat'] = get_riwayat()
    response['user'] = user
    status = Status.objects.filter(pengguna=pengguna).order_by('-id')
    response['status'] = status
    response['status_form'] = Status_Form
    return render(request, html, response)

def friends_list(request): # pragma: no cover
    friends = Teman.objects.all()
    friend_list = []

    for friend in friends:
        friend_list.append(model_to_dict(friend))

    daftar = {'friend_list' : friend_list}
    return JsonResponse(daftar)

API_RIWAYAT = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def get_riwayat(): # pragma: no cover
	riwayats = requests.get(API_RIWAYAT)
	return riwayats.json()

def edit_profile(request): # pragma: no cover
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Teman.objects.get(npm = kode_identitas)
    response['user'] = pengguna
    html = 'a2/edit.html'
    return render(request, html, response)

def update_profile(request): # pragma: no cover
    if request.method == 'POST':
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Teman.objects.get(npm = kode_identitas)
        fullname = request.POST['full_name']
        url_linkedin = request.POST['url_linkedin']
        print(url_linkedin)
        picture_url = request.POST['picture_url']
        email = request.POST['email']
        headline = request.POST['headline']
        pengguna.nama = fullname
        pengguna.url_linkedin = url_linkedin
        pengguna.foto = picture_url
        pengguna.email = email
        pengguna.headline = headline
        pengguna.save()
        response['Status_form'] = Status_Form
        kode_identitas = get_data_user(request,'kode_identitas')
        pengguna = Teman.objects.get(npm = kode_identitas)
        response['status'] = pengguna.status_set.all()
		# print(response['status'])
        stat = Status.objects.all().order_by('-id')
        if (len(stat) > 0):
            message = stat[0]
            newMessage = message.status
            time = message.created_date
        else:
            newMessage = "You have not posted yet"
            time = date(datetime.now().year,datetime.now().month,datetime.now().day)
        response['latestMessage'] = newMessage
        response['jumlah_status'] = pengguna.status_set.count()
        response['daftar_riwayat'] = views.get_daftar_riwayat()
        return HttpResponseRedirect(reverse('a2:edit_profile'))

def add_status(request): # pragma: no cover
	kode_identitas = get_data_user(request,'kode_identitas')
	pengguna = Teman.objects.get(npm = kode_identitas)
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status=response['status'], pengguna=pengguna)
		status.save()
	return HttpResponseRedirect(reverse('a2:index'))

def delete_status(request, id): # pragma: no cover
	kode_identitas = get_data_user(request,'kode_identitas')
	pengguna = Teman.objects.get(npm = kode_identitas)
	status = pengguna.status_set.filter(pk=id)
	status.delete()
	return HttpResponseRedirect(reverse('a2:index'))
