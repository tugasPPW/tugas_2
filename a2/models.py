from django.db import models
from a4.models import Teman, Keahlian

# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length = 100, null = True)
    npm = models.CharField(max_length = 20, null = True)
    mastery = models.CharField(max_length = 200, null = True)
    email = models.EmailField(max_length = 100, null = True)
    profileLinkedIn = models.URLField(max_length = 200, blank=True,null = True, default = "Kosong")

class Status(models.Model):
    pengguna = models.ForeignKey(Teman)
    status = models.CharField(max_length=1000)
    created_date = models.DateTimeField(auto_now_add=True)

    def as_dict(self): # pragma: no cover
        return {
            "status": self.status,
            "created_date": self.created_date,
        }

class Comment(models.Model):
	pengguna = models.ForeignKey(Status,on_delete=models.CASCADE)
	comment = models.CharField(max_length=1000)
	created_date = models.DateTimeField(auto_now_add=True)
