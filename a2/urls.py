from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit-profile/', edit_profile, name='edit_profile'),
    url(r'^update-profile/', update_profile, name='update_profile'),
    url(r'^delete-status/(?P<id>\d+)/$', delete_status, name='delete'),
    url(r'^add-status/', add_status, name='add'),
]
