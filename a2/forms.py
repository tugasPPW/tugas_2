from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'  write here...',
        'id': 'field'
    }
    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
