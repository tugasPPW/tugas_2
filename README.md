[![pipeline status](https://gitlab.com/tugasPPW/tugas_2/badges/master/pipeline.svg)](https://gitlab.com/tugasPPW/tugas_2/commits/master)

[![coverage report](https://gitlab.com/tugasPPW/tugas_2/badges/master/coverage.svg)](https://gitlab.com/tugasPPW/tugas_2/commits/master)

[[KELOMPOK SEMBILAN - PPW - A - FASILKOM UI - OMEGA - 2016]]

Nama Anggota Kelompok:  
  M. Ikhsan Kurniawan - 1606918433  
  Michael Giorgio Wirawan - 1606875825  
  Nabila Laili Halimah - 1606917872  
  Stanley Sebastian - 1606893494


Link Herokuapp: passeo-tissues.herokuapp.com
