from django.contrib import admin
from .models import Teman, Keahlian

# Register your models here.
admin.site.register(Teman)
admin.site.register(Keahlian)
