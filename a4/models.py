from django.db import models

# Create your models here.
class Keahlian(models.Model):
    nama = models.CharField(max_length=27)
    tingkat = models.CharField(max_length=27)

    class Meta:
        ordering = ('nama',)

    def __str__(self): # pragma: no cover
        return self.nama + self.tingkat

class Teman(models.Model):
    npm = models.CharField(max_length=10)
    name = models.CharField(max_length=400)
    angkatan = models.CharField(max_length=4)
    email = models.EmailField(max_length = 100, null = True)
    profileLinkedIn = models.URLField(max_length = 200, null = True)
    BEGINNER = 'BG'
    INTERMEDIATE = 'INT'
    ADVANCED = 'AD'
    EXPERT = 'EXP'
    LEGEND = 'LG'
    SKILL_LEVEL_CHOICES = (
        (BEGINNER, 'Beginner'),
        (INTERMEDIATE, 'Intermediate'),
        (ADVANCED, 'Advanced'),
        (EXPERT, 'Expert'),
        (LEGEND, 'Legend'),
    )
    JAVA = 'Java'
    C = 'C#'
    PYTHON = 'Python'
    ERLANG = 'Erlang'
    KOTLIN = 'Kotlin'
    PHOTOSHOP = 'Photoshop'
    SKETCH = 'Sketch'
    AOV = 'Arena of Valor'
    PALADINS = 'Paladins'
    ANAKTUHAN = 'Anak Tuhan'
    SKILL_VARIANT_CHOICES = (
        (JAVA, 'Java'),
        (C, 'C#'),
        (PYTHON, 'Python'),
        (ERLANG, 'Erlang'),
        (KOTLIN, 'Kotlin'),
        (PHOTOSHOP, 'Photoshop'),
        (SKETCH, 'Sketch'),
        (AOV, 'Arena of Valor'),
        (PALADINS, 'Paladins'),
        (ANAKTUHAN, 'Anak Tuhan'),
    )
    # skill_variant = models.CharField(
    #     max_length=20,
    #     choices=SKILL_VARIANT_CHOICES,
    #     default=ANAKTUHAN,
    # )
    # skill_level = models.CharField(
    #     max_length=3,
    #     choices=SKILL_LEVEL_CHOICES,
    #     default=BEGINNER,
    # )
    keahlian = models.ManyToManyField(Keahlian)

    class Meta:
        ordering = ('npm',)

    def __str__(self): # pragma: no cover
        return self.name
