from django.shortcuts import render
from django.forms.models import model_to_dict
from .models import Teman

# Create your views here.
response = {}

def index(request): # pragma: no cover
    response['author'] = "Michael Giorgio"
    html = 'a4/a4.html'
    friends = Teman.objects.all()
    friend_list = []

    for friend in friends:
        friend_list.append(model_to_dict(friend))

    # daftar = {'friend_list' : friend_list}
    response['friend_list'] = friend_list
    return render(request, html, response)



def friends_list(request): # pragma: no cover
    friends = Teman.objects.all()
    friend_list = []

    for friend in friends:
        friend_list.append(model_to_dict(friend))

    daftar = {'friend_list' : friend_list}
    return JsonResponse(daftar)
