from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
import requests

# Create your views here.
response = {}
def index(request):
    response['author'] = "Nabila Laili Halimah"
    response['riwayats'] = get_riwayat()
    html = "a3/riwayat-page.html"
    return render(request, html, response)

API_RIWAYAT = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def get_riwayat():
	riwayats = requests.get(API_RIWAYAT)
	return riwayats.json()