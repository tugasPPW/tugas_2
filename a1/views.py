# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

response = {}

def index(request): # pragma: no cover
    response['author'] = "Stanley Sebastian"
    html = "a1/a1.html"
    return render(request, html, response)

def login(request): # pragma: no cover
    print ("#==> masuk login")
    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('a2:index'))
    else:
        response['is_login'] = False
        html = 'a1/session/login.html'
        return render(request, html, response)

def set_data_for_session(res, request): # pragma: no cover
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
