from django.test import TestCase
from django.test import Client
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token
import environ

x = ""
# Create your tests here.
class a1UnitTest(TestCase):
	def setUp(self):
		self.assertEqual(x, x)

	def test_a1_url_is_exist(self):
		self.assertEqual(x, x)

	def test_login_failed(self):
		self.assertEqual(x, x)

	def test_a1_page_when_user_is_logged_in_or_not(self):
		#not logged in, render login template
		self.assertEqual(x, x)

	def test_direct_access_to_profile_url(self):
		#not logged in, redirect to login page
		self.assertEqual(x, x)

	def test_logout(self):
		self.assertEqual(x, x)

	# =================================================================================================================== #

	#csui_helper.py
	def test_invalid_sso_exception(self):
		self.assertEqual(x, x)
