"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import a1.urls as a1
import a2.urls as a2
import a3.urls as a3
import a4.urls as a4


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^a1/', include(a1, namespace='a1')),
    url(r'^a3/', include(a3, namespace='a3')),
    url(r'^a2/', include(a2, namespace='a2')),
    url(r'^a4/', include(a4, namespace='a4')),
    url(r'^$', RedirectView.as_view(url='/a1/', permanent = 'true'), name = 'a1')
    ]
